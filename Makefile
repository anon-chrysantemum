%.html: %.html.template
	awk '{line = $$0; if (match($$0, /<IMPORT (.*)\/>/, ary) != 0) system("cat " ary[1]); else print line}' $< > $@

chryzantem.html: petals.html center.html face.html stem.html text.html

_inner_row_delay _flower_delay petals.html: make_petals.sh
	./make_petals.sh > petals.html

center.html: make_center.sh _inner_row_delay
	./make_center.sh > center.html

_face_delay face.html: make_face.sh _flower_delay
	./make_face.sh > face.html

_stem_delay stem.html: make_stem.sh _face_delay
	./make_stem.sh > stem.html

text.html: make_text.sh _stem_delay
	./make_text.sh > text.html

clean:
	rm -rf *_delay petals.html center.html face.html stem.html text.html chryzantem.html

.PHONY: clean
