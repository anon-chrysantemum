#!/bin/sh

ANIMATION_DELAY=$(bc <<EOF
$(cat _flower_delay) - 0.15
EOF
		  )

cat <<EOF
      <div class="smile">
	<img style="animation-delay: ${ANIMATION_DELAY}s;" src="./smile.svg" alt="flower smile" draggable="false">
      </div>
      <div class="eyes">
	<img style="animation-delay: ${ANIMATION_DELAY}s;" src="./eyes.svg" alt="flower eyes" draggable="false">
      </div>
EOF

FACE_APPEAR_DURATION=0.3 # must match animation-duration in chryzantem.html.template

bc >_face_delay <<EOF
$ANIMATION_DELAY + $FACE_APPEAR_DURATION
EOF
