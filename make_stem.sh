#!/bin/sh

ANIMATION_DELAY=$(bc <<EOF
$(cat _face_delay) + 0.2
EOF
		  )

cat <<EOF
      <div class="stem" style="animation-delay: ${ANIMATION_DELAY}s;"></div>
EOF

STEM_APPEAR_DURATION=0.6 # must match animation-duration in chryzantem.html.template

bc >_stem_delay <<EOF
$ANIMATION_DELAY + $STEM_APPEAR_DURATION
EOF
